# HashiCorp Nomad install

This role installs the Nomad agent in server or client mode. It is mostly based
on the [deployment guide](https://developer.hashicorp.com/nomad/tutorials/enterprise/production-deployment-guide-vm-with-consul).

## How to use

The role is tested with basic config on Arch Linux, Ubuntu and AlmaLinux.
I'm not testing a full client/server setup, so don't expect it to "just work".

## How to test

You'll need [Molecule](https://molecule.readthedocs.io/en/latest/installation.html),
the `podman` plugin for Molecule and `ansible-lint`.

Run `molecule test` in the root path of this repository.

The default Molecule test OS is Arch Linux. To run tests on Ubuntu/AlmaLinux,
set the env var `ROLE_TEST_OS` to `ubuntu` or `almalinux`.

## Should I use it?

<!--- yeah I copy pasted from my Consul README --->

This role is not meant to be "production ready", but it is good enough to play
with Nomad on a hobby project. Read the additional considerations before making
your decision.

If you need a more customizable role, check out [Ansible Galaxy](https://galaxy.ansible.com/).

## Additional considerations

### Extra configuration

Nomad's configuration is different from Consul, so I added three variables for
extra configuration:

- `nomad_extra_configuration`: applies to the [general](https://developer.hashicorp.com/nomad/docs/configuration)
agent config.
- `nomad_client_extra_configuration` and `nomad_server_extra_configuration`:
applies to [client](https://developer.hashicorp.com/nomad/docs/configuration/client)
and [server](https://developer.hashicorp.com/nomad/docs/configuration/server) blocks, respectively.

Values should be written in HCL format, check the example in [`defaults`](./defaults/main.yaml).

You can use that to enable the GUI, auto-join, plugins, or any other settings not
covered in the [configuration template](./templates/nomad.hcl.j2).

### Gossip encryption

Encryption is used by servers only, check the
[documentation](https://developer.hashicorp.com/nomad/tutorials/transport-security/security-gossip-encryption)
for more information.

The key cannot be rotated by just changing the variable value.
[Docs](https://developer.hashicorp.com/nomad/docs/configuration/server#encrypt):
> If it is provided after Nomad has been initialized with an encryption key,
> then the provided key is ignored and a warning will be displayed.

This role will not do anything to rotate encryption keys for you. You'll need to
do manual intervention.

### TLS encryption

This role will not create the CA and agent certificates for you. You must do it separately
and add the correct variables to each server/client inventory.

The process for adding TLS Encryption to an already provisioned cluster can be found
in the [documentation](https://developer.hashicorp.com/nomad/tutorials/transport-security/security-enable-tls).

This role has been tested on new clusters only.

The molecule test scenarios for TLS contain exposed certificate keys. Don't use them
for anything besides running those tests.

### ACL

Setting up the ACL system requires manual intervention, there are no tasks in this role
to help with the setup.

Read the [docs](ihttps://developer.hashicorp.com/nomad/tutorials/access-control/access-control-bootstrap)
for more information on how to do it.
